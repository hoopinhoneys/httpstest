view: students {
  sql_table_name: nested_practice_data.students ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: class {
    hidden: yes
    sql: ${TABLE}.class ;;
  }

  dimension: first_name {
    type: string
    sql: ${TABLE}.first_name ;;
  }

  dimension: last_name {
    type: string
    sql: ${TABLE}.last_name ;;
  }

  measure: count {
    type: count
    drill_fields: [id, last_name, first_name]
  }
}

view: students__class {
  dimension: grade {
    type: string
    sql: ${TABLE}.grade ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: teacher_id {
    type: number
    sql: ${TABLE}.teacher_id ;;
  }
}
